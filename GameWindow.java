import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

	/**
	* This class creates the Window for the game to be held in.
	* It also is the listener to record which buttons are pressed.
	*
	*
	* @author Matthew Marsden
	*/
public class GameWindow implements ActionListener {
		
	public static final int SCREEN_WIDTH = 800;
    public static final int SCREEN_HEIGHT = 650;
	
	
	private JLabel scre = new JLabel("0");
	private JButton strt = new JButton("Start");
	private JButton pse = new JButton("Pause");
	private JButton stp = new JButton("Stop");
	
	private Racer r;
	private boolean paused = false;
	
	public GameWindow()
	{
		r = new Racer();
	}

	public Racer getRacer()
	{
		return r;
	}
	
	/**
	 * Creates the JFrame with the elements inside that allow
	 * the game to fit inside.
	 *
	 *
	 */
	public void createWindow()
	{
		
		JFrame window = new JFrame();
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel gameWindow = new JPanel();
		JPanel startStop = new JPanel();
     	JPanel score = new JPanel();
		
		BorderLayout gameLayout = new BorderLayout();
		GridLayout scoreLayout = new GridLayout(0, 8);
		GridLayout startStopLayout = new GridLayout(0, 3);
		
		JLabel a = new JLabel("Current Score: ");
		
		strt.addActionListener(this);
		pse.addActionListener(this);
		stp.addActionListener(this);
		
		score.setLayout(scoreLayout);
		score.add(a);
		score.add(scre);
		
		startStop.setLayout(startStopLayout);
		startStop.add(strt);
		startStop.add(pse);
		startStop.add(stp);
		
		gameWindow.setLayout(gameLayout);
		gameWindow.add("North", score);
		gameWindow.add("Center", r.getPanel());
		gameWindow.add("South", startStop);
		
        window.setTitle("Super race challenge");
        window.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
        window.setContentPane(gameWindow);
		
        window.setVisible(true);

        r.getScore(scre);

        r.start();
        gameLoop();
	}

		/**
		 * The actionListener to check which button has been pressed by the user.
		 * @param e
		 */
	public void actionPerformed(ActionEvent e)
	{

		if(e.getSource() == strt)
		{
			r.stop();
			r.start();

		}
		else if(e.getSource() == pse)
		{
			r.setPause(!paused);
			paused = !paused;
			
		}
		else if(e.getSource() == stp)
		{
			r.stop();
		}
			
	}

		/**
		 * For sending the score label to other classes.
		 * @return
		 */
	public JLabel getScoreLabel()
	{
		return scre;
	}

		/**
		 * Called to when we want to run the game.
		 */
	public void gameLoop()
	{
		r.begin();

	}
	
}