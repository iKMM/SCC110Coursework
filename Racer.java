import javax.swing.*;
/**
 * uses the GameArena APIs to implement a simple top down racing game.
 *
 * The graphical output of the game is provided as a Swing component,
 * so that it can be added into any Swing application, just like a JButton etc.
 *
 * To allow users to control the game as they see fit, start(), stop() and update()
 * methods are provided. start() should be used to create a new game, stop() to terminate
 * a running game, and update() should be called in a loop to update gameplay and graphics
 *
 * Simple example of use:
 *
 * <pre>
 *
 *  JFrame window = new JFrame();
 *  Racer r = new Racer();
 *  window.setTitle("Racer");
 *  window.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
 *  window.setContentPane(r.getPanel());
 *  window.setVisible(true);
 *
 *  r.start();
 *
 *  while(r.isPlaying())
 *      r.update();
 *
 * </pre>
 *
 * @author Joe Finney (joe@comp.lancs.ac.uk)
 */
public class Racer
{
    public static final double PLAYER_SPEED = 5;
    public static final int ROAD_SEGMENT_WIDTH = 160;
    public static final int ROAD_SEGMENT_HEIGHT= 10;
    public int ROAD_CURVE_SPEED = 4;
	public static final int SCREEN_WIDTH = 800;
    public static final int SCREEN_HEIGHT = 600;

    private GameArena arena;
    private Car player;
    private RoadSegment[] road = new RoadSegment[SCREEN_HEIGHT / ROAD_SEGMENT_HEIGHT + 1];

    private double currentRoadX = SCREEN_WIDTH/2;
    private double speed = 2.0;
    private boolean playing = false;
    private int score = 0;
	private boolean paused = false;
	private JLabel scre;

    /**
     * Creates a new instance of the Racer racing game.
     */
    public Racer()
    {
        arena = new GameArena(SCREEN_WIDTH, SCREEN_HEIGHT, false);
    }

    /**
     * Provides a Swing component in which the Racer game runs.
     * This component can be added to a Swing panel to display the game on screen.
     *
     * @return A Swing component for this game.
     */
    public JComponent getPanel()
    {
        return arena.getPanel();
    }

    /**
     * Provides the player's current score in the game.
     * @return The player's current score.
     */
    public int getScore()
    {
        return score;
    }

    /**
     * Starts a new game, if the game is not already running.
	 * Continues the game otherwise
     */
    public void start()
    {
		if(paused)
		{
			paused = false;
			playing = false;
		}
		
		
		
        if(!playing)
        {
            // Create the player's car
            player = new Car(SCREEN_WIDTH/2, SCREEN_HEIGHT - 150, arena);

            // Create the initial road layout
            for (int s = road.length-1; s >= 0 ; s--)
            {
                road[s] = nextRoadSegment(); 
                road[s].setYPosition(s*ROAD_SEGMENT_HEIGHT);
            }

            score = 0;
            playing = true;
			ROAD_CURVE_SPEED = 2;
			speed = 2.0;
        }
		
    }

    /**
     * Stops a currently running game.
     */
    public void stop()
    {
        if(playing)
        {
            playing = false;
            arena.exit();
        }
    }
	
    /**
     * Determines if the game is currently being played.
     *
     * @return false if the game has not been started or on game over, true if the game is actively running.
     */
    public boolean isPlaying()
    {
        return playing;
    }

    /**
     * Updates the game state to allow the road and player character to move on the screen.
     *
     * This method should be called in a loop (once per frame) to advance gameplay in response to time
     * and user input. The method uses the GameArena pause() method to ensure the game runs at a constant speed.
     */
    public void update()
    {
        if(playing && !paused)
        {
            score++;
			
			scre.setText(String.valueOf(score));
			
            double speed = 0;
            if (arena.leftPressed())
                speed -= PLAYER_SPEED;

            if (arena.rightPressed())
                speed += PLAYER_SPEED;

            player.setXSpeed(speed);

            player.move();
            for (int i=0; i<road.length; i++)
            {
                if (road[i] != null)
                    road[i].move();
            }

            // Recycle any segments that have crolled off screen...
            recycleRoadSegments();

            if (hasCrashed())
                stop();
			
        }
		
        arena.pause();
		
    }

	public void getScore(JLabel scre)
    {
        this.scre = scre;
    }
	
	public void setPause(boolean paused)
	{
		this.paused = paused;
	}
	


    /**
     * Provides a randomly generated, thin slice of road. 
     * This method is used periodically to create new road on the screen in front of the player's car.
     * If it looks like the world is ending all around the car, it's working as intended.
     * @return A new randomly generated RoadSegment
     */
    private RoadSegment nextRoadSegment() {
        double curveParam = 2.1;
		

        if (score > 200 && score < 600)
        {
            ROAD_CURVE_SPEED = 1;
            speed = 3.0;
            curveParam = 3;
        }

        if (score > 600)
        {
            ROAD_CURVE_SPEED = 9;
            speed = 5.0;
            curveParam = 2;
        }

        if (score > 1200)
        {
            ROAD_CURVE_SPEED = 9;
            speed = speed + 1;
            curveParam = 2;
        }

        currentRoadX += Math.random() * curveParam * ROAD_CURVE_SPEED - ROAD_CURVE_SPEED;
        RoadSegment s = new RoadSegment(currentRoadX, -ROAD_SEGMENT_HEIGHT, ROAD_SEGMENT_WIDTH, ROAD_SEGMENT_HEIGHT, arena);
        s.setYSpeed(speed);
        return s;
    }

    /**
     * Removes any parts of road that have scrolled off the bottom of the screen.
     */
    private void recycleRoadSegments()
    {
        for (int i=0; i<road.length; i++)
        {
            if (road[i].getYPosition() > SCREEN_HEIGHT)
            {
                double y = road[i].getYPosition();
                road[i].remove();
                road[i] = nextRoadSegment(); 
                road[i].setYPosition(y - SCREEN_HEIGHT - ROAD_SEGMENT_HEIGHT);
            }
        }
    }

    /**
     * Determines if the player has crased (driven off road)
     *
     * @return true is the player is touching the kerb/grass, false otherwise.
     */
    private boolean hasCrashed()
    {
        for (int i=0; i<road.length; i++)
        {
            if (player.isTouching(road[i]))
                return true;
        }

        return false;
    }
	
	public void begin()
	{
		start();
		while(true)
		{
			arena.pause();
			while(playing)
			{
				update();
			}
		}
		
	}
	
}
