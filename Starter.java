import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
	* This class is the main game controller.
	* It creates the windows for the game and dictates which part is in use.
	*
	*
	* @author Matthew Marsden
	*/
public class Starter {
	
	public static void main(String[] args)
	{
		
		
		GameWindow game = new GameWindow();

		Scoreboard hiscores = new Scoreboard(); 					
		hiscores.makeScoreboard();
		hiscores.arrangeScores();
		
		Racer r = game.getRacer();

		hiscores.setScore(r);

		game.createWindow();
		
		game.gameLoop();
	}
	
}