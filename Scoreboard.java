import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * This class creates a scoreboard for players to save their high scores to.
 * It creates a seperate JFrame with panels for entering the name
 * and a panel to view the scores.
 *
 * @author Matthew Marsden
 */
public class Scoreboard implements ActionListener {



	public static final int SCREEN_WIDTH = 300;
	public static final int SCREEN_HEIGHT = 300;

	private int scores[] = new int[6];
	private String names[] = new String[6];
	private int index = 0;

	private JTextField nameBox = new JTextField("");
	private JButton confirmScore = new JButton("Confirm");

	private JPanel scoreWindow = new JPanel();

	private Racer r = new Racer();
	private JPanel windowHolder = new JPanel();
	private JFrame hiscores = new JFrame();

	public Scoreboard()
	{

	}

	/**
	 * The method used to build the scoreboard and create the general layout
	 *
	 */
	public void makeScoreboard()
	{

		hiscores.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		hiscores.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);



		JPanel nameWindow = new JPanel();

		BorderLayout windowLayout = new BorderLayout();
		GridLayout scoreLayout = new GridLayout(8, 2);
		GridLayout nameLayout = new GridLayout(0, 3);

		windowHolder.setLayout(windowLayout);
		scoreWindow.setLayout(scoreLayout);
		nameWindow.setLayout(nameLayout);

		JLabel nameLabel = new JLabel("Your name: ");


		nameWindow.add(nameLabel);
		nameWindow.add(nameBox);
		nameWindow.add(confirmScore);

		confirmScore.addActionListener(this);

		windowHolder.add("North", nameWindow);
		windowHolder.add("Center", scoreWindow);

		hiscores.setContentPane(windowHolder);
		hiscores.setVisible(true);
		hiscores.setTitle("High scores");


	}

	/**
	 * Gives the score based on the score in the racer.
	 * @param r
	 */
	public void setScore(Racer r)
	{
		this.r = r;
	}

	/**
	 * Saves the current score achieved.
	 */
	public void addScore()
	{
		if(index == 6)
		{
			index = 5;
		}
		scores[index] = r.getScore();
		names[index] = nameBox.getText();

		String scoreText = String.valueOf(scores[index]);
		String nameText = names[index];

		JLabel score = new JLabel(scoreText);
		JLabel name = new JLabel(nameText);

		scoreWindow.add(name);
		scoreWindow.add(score);

		hiscores.setContentPane(windowHolder);

		scoreWindow.revalidate();
		scoreWindow.repaint();

		index++;
	}

	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource() == confirmScore)
		{
			addScore();
			arrangeScores();
		}
	}


	/**
	 * A sorting algorithm which orders the scores highest to lowest.
	 * Always kicks the last person off the table and replaces it with the latest score.
	 * If the latest score is higher than any of the other scores it reorders the scores.
	 */
	public void arrangeScores()
	{
		String namePlaceholder = "";
		int sortPlaceholder = 0;
		boolean sorted = true;

		while(sorted)
		{
			sorted = false;
			for(int i = 0; i < scores.length-1; i++)
			{
				if(scores[i] < scores[i+1])
				{
					sortPlaceholder = scores[i];
					namePlaceholder = names[i];
					scores[i] = scores[i + 1];
					names[i] = names[i + 1];
					scores[i+1] = sortPlaceholder;
					names[i+1] = namePlaceholder;
					sorted = true;
				}
			}
		}

		scoreWindow.removeAll();

		for(int i = 0; i < scores.length; i++)
		{
			String scoreText = String.valueOf(scores[i]);
			String nameText = names[i];

			JLabel score = new JLabel(scoreText);
			JLabel name = new JLabel(nameText);

			scoreWindow.add(name);
			scoreWindow.add(score);
		}


		hiscores.setContentPane(windowHolder);

		scoreWindow.revalidate();
		scoreWindow.repaint();
	}

}